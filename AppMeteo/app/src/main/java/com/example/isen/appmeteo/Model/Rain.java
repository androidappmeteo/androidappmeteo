package com.example.isen.appmeteo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by isen on 22/12/2015.
 */
public class Rain {
    @SerializedName("3h")
    private double previous3h;

    public double getPevious3h() {
        return previous3h;
    }

    public void setPevious3h(double pevious3h) {
        this.previous3h = pevious3h;
    }
}
