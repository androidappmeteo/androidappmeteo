package com.example.isen.appmeteo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by isen on 22/12/2015.
 */
public class Clouds {
    @SerializedName("all")
    private double cloudiness;

    public void setCloudiness(int cloudiness) {
        this.cloudiness = cloudiness;
    }

    public double getCloudiness() {
        return cloudiness;
    }
}