package com.example.isen.appmeteo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by isen on 22/12/2015.
 */
public class Snow {
    @SerializedName("3h")
    private double previous3h;

    public double getPrevious3h() {
        return previous3h;
    }

    public void setPrevious3h(int previous3h) {
        this.previous3h = previous3h;
    }
}
